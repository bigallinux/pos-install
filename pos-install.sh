#!/bin/bash

echo '******* RMPFUSION ********'
sudo dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
echo '******* END Install *******'

echo '******* FireFox ********'
sudo dnf -y install firefox
echo '******* End Install *******'

echo '******* ARK *******'
sudo dnf -y install ark
echo '******* End Install *******'

echo '******* Libre Office para portugues brasil ********'
sudo dnf -y install libreoffice-langpack-pt-BR
echo '******* End Install ********'

echo '******* Telegram *******'
sudo dnf -y install telegram
echo '******* End Install *******'

echo '******* GIMP *******'
sudo dnf -y install gimp
echo '******* End Install *******'

echo '******* kcolorchooser *******'
sudo dnf -y install kcolorchooser
echo '******* End Install *******'

echo '******* KTorrent *******'
sudo dnf -y install ktorrent
echo '******* End Install *******'

echo '******* Filezilla *******'
sudo dnf -y install filezilla
echo '******* End Install *******'

echo '******* kdeconnectd *******'
sudo dnf -y install kdeconnectd
echo '******* End Install *******'

echo '******* XSane ********'
sudo dnf -y install xsane
echo '******* End Install *******'

